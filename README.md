# Setup config for Lingua Libre

Ansible playbooks, inventory, and configuration for various services, to easily deploy Lingua Libre.

The following Ansible playbooks are available:
* **[VAULT]** `setup.yaml` - Sets up the Lingua Libre Beta Cluster deployment on `development` hosts.
* `update.yaml` - Updates Lingua Libre on `development` hosts.

## Nginx

The Nginx config for both `dev.lingualibre.org` and `lingualibre.org` are quite strict on the front-end side: this is to mitigate any risks of a 3rd-party bad actor getting access to the OAuth token of an user and using it for any malicious intent.
Also, most of the security headers enforced here are considered **good practice** to prevent XSS or MITM attacks.
As such, you should regularly check the configuration against [Google's CSP Evaluator](https://csp-evaluator.withgoogle.com/) or [Security Headers by Probely](https://securityheaders.com/).

With the `Content-Security-Policy` header, the configuration only allows the browser to connect to the following URLs:
* `lingualibre.org`
* `*.lingualibre.org`
* `wikidata.org` (Wikidata API)
* `*.wikidata.org` (Wikidata API)

That list is to be kept as short as possible.
If you ever need Lingua Libre's front-end to connect to another 3rd-party service, you must add the 3rd-party's domain name to the `connect-src` list.
Otherwise, connections that worked fine on your localhost and betacluster environments will suddenly stop working when deploying to production 😉.

**NOTE:** localhost, due to it not being served by nginx (obviously), do not use the CSP configuration.
The *betacluster* deployment, however, uses the `Content-Security-Policy-Report-Only` header.
It applies the same rules, but does not prevent the requests from being executed.
However, they leave a very noticeable error in your web browser's console (!) which **should help you catch any issues with the CSP config _before_ they reach production**.

## Usage

* `ansible-playbook -i inventory.yaml setup.yaml --ask-vault-pass`

### Requirements

* Python 3
* Ansible

**TESTED ONLY ON DEBIAN 12**
